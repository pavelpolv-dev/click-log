import React from 'react'
import block from 'bem-cn'
import './App.scss'
import { ButtonClickLogContainer } from 'containers/ButtonClickLog'

const cn = block('main')
const App = () => {
    return (
        <div className={cn()}>
            <ButtonClickLogContainer />
        </div>
    )
}

export default App
