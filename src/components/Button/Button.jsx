import React from 'react'
import PropTypes from 'prop-types'
import block from 'bem-cn'

import './style.scss'

const cn = block('btn')

const propTypes = {
    children: PropTypes.any,
    color: PropTypes.oneOf(['primary', 'danger']),
    onClick: PropTypes.func,
}

const defaultProps = {
    color: 'primary',
}

const Button = ({ children, color, onClick }) => {
    return (
        <button
            className={cn({ color })}
            onClick={onClick}
        >
            {children}
        </button>
    )
}

Button.propTypes = propTypes
Button.defaultProps = defaultProps

export default React.memo(Button)
