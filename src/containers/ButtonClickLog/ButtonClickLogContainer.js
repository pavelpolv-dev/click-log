import React, { useEffect, useState, useRef, useCallback } from 'react'
import { ButtonClickLog } from './components/ButtonClickLog'

const getRandom = () => Math.floor(Math.random() * 9) + 1
const currentDate = () => {
    const date = new Date()

    return date.getTime()
}

const ButtonClickLogContainer = () => {
    const [ activeAction, setActiveAction ] = useState({})
    const timerRef = useRef(null)
    const logsRef = useRef([])
    const queueRef = useRef([])

    const handleSetLog = () => {
        const [firstAction, ...otherAction] = queueRef.current

        timerRef.current = null
        queueRef.current = otherAction || []
        logsRef.current = [...logsRef.current, { ...firstAction, dateOutput: currentDate() }]
        setActiveAction({})
    }

    const handleStartTimer = () => {
        const { delay } = activeAction

        timerRef.current = setTimeout(handleSetLog, delay * 1000)
    }

    useEffect(() => {
        if(!timerRef.current && Object.keys(activeAction).length) {
            handleStartTimer()
        }
        if (!Object.keys(activeAction).length && queueRef.current.length) {
            const [firstAction, ...otherAction] = queueRef.current // eslint-disable-line no-unused-vars

            setActiveAction(firstAction)
        }
    }, [activeAction])

    const handleSetAction = useCallback((btnName) => {
        const actionParams = {
            delay: getRandom(),
            btnName,
            dateClick: currentDate(),
        }

        queueRef.current = [...queueRef.current, actionParams]
        if (!Object.keys(activeAction).length) {
            setActiveAction(actionParams)
        }
    }, [])

    const handleReset = useCallback(() => {
        clearTimeout(timerRef.current)
        timerRef.current = null
        logsRef.current = []
        queueRef.current = []
        setActiveAction({})
    }, [])

    return (
        <ButtonClickLog
            onSetAction={handleSetAction}
            onReset={handleReset}
            itemsLog={logsRef.current}
        />
    )
}

export default ButtonClickLogContainer
