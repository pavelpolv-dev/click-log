import React from 'react'
import PropTypes from 'prop-types'
import block from 'bem-cn'

import { ButtonsRow } from '../ButtonsRow'
import { ClickLogs } from '../ClickLogs'
import { FooterLog } from '../FooterLog'
import './style.scss'

const propTypes = {
    itemsLog: PropTypes.array,
    onReset: PropTypes.func,
    onSetAction: PropTypes.func,
}
const cn = block('button-click-log')

const ButtonClickLog = ({ itemsLog, onSetAction, onReset }) => (
    <div className={cn()}>
        <ButtonsRow onSetAction={onSetAction} />
        <ClickLogs itemsLog={itemsLog} />
        <FooterLog onReset={onReset} />
    </div>
)


ButtonClickLog.propTypes = propTypes

export default ButtonClickLog
