import React from 'react'
import PropTypes from 'prop-types'
import block from 'bem-cn'

import { Button } from 'components/Button'

import './style.scss'

const cn = block('button-rows')
const propTypes = {
    onSetAction: PropTypes.func,
}
const ButtonsRow = ({ onSetAction }) => {
    const handleClick = ({ target }) => {
        const { textContent } = target

        onSetAction(textContent)
    }

    return (
        <div className={cn()}>
            {
                [...Array(3)].map((_, key) => (
                    <Button
                        key={key}
                        onClick={handleClick}
                    >
                        {`Button ${++key}`}
                    </Button>
                ))
            }
        </div>
    )
}

ButtonsRow.propTypes = propTypes

export default React.memo(ButtonsRow)
