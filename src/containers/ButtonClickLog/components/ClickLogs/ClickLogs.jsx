import React from 'react'
import PropTypes from 'prop-types'
import block from 'bem-cn'
import { Scrollbars } from 'react-custom-scrollbars';

import ItemLog from './ItemLog'
import EmptyLog from './EmptyLog'
import './style.scss'

const cn = block('click-logs')
const propTypes = {
    itemsLog: PropTypes.array,
}
const ClickLogs = ({ itemsLog }) => {
    return (
        <div className={cn()}>
            <Scrollbars>
                {itemsLog.length ?
                    itemsLog.map((dataLog, key) => (
                        <ItemLog
                            key={key}
                            {...dataLog}
                        />
                    )) : <EmptyLog cn={cn} />
                }
            </Scrollbars>
        </div>
    )
}

ClickLogs.propTypes = propTypes

export default ClickLogs
