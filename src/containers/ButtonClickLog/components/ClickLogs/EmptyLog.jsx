import React from 'react'
import PropTypes from 'prop-types'

const propTypes = {
    cn: PropTypes.func,
}
const EmptyLog = ({ cn }) => {
    return (
        <div className={cn('empty-log')}>
             Лог пустой
        </div>
    )
}

EmptyLog.propTypes = propTypes

export default EmptyLog
