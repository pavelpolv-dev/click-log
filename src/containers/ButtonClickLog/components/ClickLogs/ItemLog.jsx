import React from 'react'
import PropTypes from 'prop-types'
import { dataToTimestamp } from 'utils/date'

const propTypes = {
    btnName: PropTypes.string,
    dateClick: PropTypes.number,
    dateOutput: PropTypes.number,
    delay: PropTypes.number,
}
const ItemLog = ({ delay, btnName, dateClick, dateOutput }) => {
    return (
        <div>
            {`${dataToTimestamp(dateOutput)}: Нажата кнопка ${btnName}, задержка ${delay}, время нажатия 
            ${dataToTimestamp(dateClick)}`}
        </div>
    )
}

ItemLog.propTypes = propTypes

export default ItemLog
