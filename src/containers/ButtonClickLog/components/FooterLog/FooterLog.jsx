import React from 'react'
import PropTypes from 'prop-types'
import block from 'bem-cn'

import { Button } from 'components/Button'
import './styles.scss'

const cn = block('footer-logs')
const propTypes = {
    onReset: PropTypes.func,
}
const FooterLog = ({ onReset }) => {
    return (
        <div className={cn()}>
            <Button
                color="danger"
                onClick={onReset}
            >
                Сбросить сосотояние
            </Button>
        </div>
    )
}

FooterLog.propTypes = propTypes

export default React.memo(FooterLog)
