/* eslint-disable max-len */

const correctParams = (number) => {
    return number < 10 ? `0${number}` : number
}

/**
 * Приведение даты к формату TIMESTAMP
 * @param time {string|number}
 * @returns {string}
 */
export const dataToTimestamp = (time) => {
    const date = new Date(time)

    return `${date.getFullYear()}-${correctParams(date.getMonth() + 1)}-${correctParams(date.getDay())} ${correctParams(date.getHours())}:${correctParams(date.getMinutes())}:${correctParams(date.getSeconds())}`
}

/**
 * Текущая дата
 * @returns {number}
 */
export const currentDate = () => {
    const date = new Date()

    return date.getTime()
}
